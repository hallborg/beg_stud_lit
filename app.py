from flask import Flask
from flask import request
from flask import jsonify
from flask import make_response
from flask import render_template
from flask.ext.autodoc import Autodoc

import requests
from modules import autodoc_json
from modules import mariadb_connection
from modules import mariadb_functions as maria_f
from modules import isbn_search
app = Flask(__name__)
auto = Autodoc(app)

# @app.route(arg) creates a path to the specified arg
# @auto.doc() takes the route and function where it's located and
# creates an object of it for the documentation


@app.route('/')
@app.route('/<string:name>')
@auto.doc()
def index(name=None):
    '''first page of the application
    Use render_template to render html files.
    see templates/index.html'''
    return render_template('index.html', name=name)


@app.route('/docs')
@auto.doc()
def doc_html():
    '''sends the auto generated documentation page'''
    return auto.html()


@app.route('/docs/json')
@auto.doc()
def doc_json():
    '''sends the auto generated documentation in JSON'''
    data = autodoc_json.generate_json(auto.generate())
    response = make_response(
        jsonify(items=data),
        200
    )
    return response


@app.route('/search/<string:isbn>')
@auto.doc()
def search_isbn(isbn):
    obj, status_code = maria_f.search_book(isbn)
    if status_code == 200:
        return make_response(jsonify(obj), status_code)
    else:
        return make_response(obj, status_code)


@app.route('/user/<string:user_name>', methods=['GET'])
@auto.doc()
def show_user(user_name):
    '''Show user by name'''
    obj, status_code = maria_f.get_user(user_name)
    return make_response(jsonify(obj), status_code)


@app.route('/user/<int:id>', methods=['DELETE'])
@auto.doc()
def delete_user(id):
    '''Example on RESTful endpoint - delete user by id'''
    # notice how the uri for this function is the same as show_user
    return 'Deleted user ' + str(id)


@app.route('/user', methods=['POST'])
@auto.doc()
def create_user():
    '''Create a new user'''
    user_name = request.form.get('user_name')
    password = request.form.get('password')
    email = request.form.get('email')
    phone_nr = request.form.get('phone_nr')
    location = request.form.get('location')
    msg, status_code = maria_f.create_user(
        user_name,
        password,
        email,
        phone_nr,
        location
    )
    return make_response(msg, status_code)


@app.route('/book/add/<string:user_name>/<string:isbn>')
@auto.doc()
def add_book(user_name, isbn):
    '''Add a book to a user'''
    return maria_f.link_book_to_user(user_name, isbn)


@app.route('/login', methods=['POST'])
@auto.doc()
def login_user():
    '''Try to login a user'''
    user_name = request.form.get('user_name')
    password = request.form.get('password')
    msg, status_code = maria_f.verify_login(user_name, password)
    return make_response(msg, status_code)


if __name__ == '__main__':
    app.run(debug=True)
