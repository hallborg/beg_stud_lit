from modules import orchestrate_functions as orch_f
from modules import mariadb_functions as maria_f
import requests

status_failed = 400
status_ok = 200
status_created = 201


# Using local database (mariadb)
def test_create_user():
    user_name = 'TestUser'
    password = '***'
    email = 'test-email'
    phone_nr = 'test-phone'
    location = 'test-location'
    msg, status_code = maria_f.create_user(
        user_name,
        password,
        email,
        phone_nr,
        location
    )
    assert status_created == status_code


def test_login():
    user_name_ok = 'TestUser'
    password_ok = '***'
    user_name_bad = 'root'
    password_bad = 'root'
    msg_1, status_code_ok = maria_f.verify_login(user_name_ok, password_ok)
    msg_2, status_code_bad = maria_f.verify_login(user_name_bad, password_bad)
    msg_3, status_code_mix1 = maria_f.verify_login(user_name_bad, password_ok)
    msg_4, status_code_mix2 = maria_f.verify_login(user_name_ok, password_bad)

    assertor(status_code_ok, status_ok)
    assertor(status_code_bad, status_failed)
    assertor(status_code_mix1, status_failed)
    assertor(status_code_mix2, status_failed)


def test_get_user():
    user_name = 'TestUser'
    obj = {
        'email': 'test-email',
        'phone_nr': 'test-phone',
        'location': 'test-location'
    }
    resp_obj, status_code = maria_f.get_user(user_name)
    assert obj == resp_obj


def test_search_book():
    msg_1, s_c_short_number = maria_f.search_book('123456789')
    msg_2, s_c_ok = maria_f.search_book('9780345539434')
    msg_3, s_c_not_found = maria_f.search_book('9330345539111')

    assertor(s_c_short_number, status_failed)
    assertor(s_c_ok, status_ok)
    assertor(s_c_not_found, status_failed)


# Using orchestrate
# Will keep this here but it wont be used.
# TESTS ARE JUST FOR FUN
def create_user():
    user_name = 'TestUser'
    password = '***'
    email = 'test-email'
    phone_nr = 'test-phone'
    location = 'test-location'
    status_code = orch_f.create_user(
        user_name,
        password,
        email,
        phone_nr,
        location
    )
    assert status_code == 201


def delete_user():
    status_code = orch_f.delete_user('TestUser')
    assert status_code == 204


def assertor(status_code, comparison):
    if status_code == comparison:
        assert True
    else:
        assert False
