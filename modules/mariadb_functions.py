from flask import make_response
from flask import jsonify

import mariadb_connection
import ConfigParser
import hashlib
import isbn_search
db = mariadb_connection.db
# cursor = mariadb_connection.cursor
config = ConfigParser.ConfigParser()
# Server side
# Don't have a cfg file yet
# local use (surface)
# cfg_path = 'C:\Users\Jesper\github\cfg\cfg.ini'
# Test path
cfg_path = 'tests/test_cfg.ini'


def create_user(user_name, password, email, phone_nr, location):
    cursor = db.cursor()
    password = salted_password(password)
    query = 'call create_user({0}, {1}, {2}, {3}, {4})'
    query = query.format(
        "'" + user_name + "'",
        "'" + password + "'",
        "'" + email + "'",
        "'" + phone_nr + "'",
        "'" + location + "'"
    )
    try:
        cursor.execute(query)
        db.commit()
        cursor.close()
        return 'account created', 201
    except Exception, e:
        if 'Duplicate entry' not in str(e):
            cursor.close()
            return 'server error', 500
        cursor.close()
        return 'User already exists', 400


def get_user(user_name):
    cursor = db.cursor()
    query = 'SELECT email, phone_nr, location \
    FROM student \
    WHERE user_name = {0}'
    cursor.execute(query.format("'" + user_name + "'"))
    res = cursor.fetchall()[0]
    obj = {
        'email': res[0],
        'phone_nr': res[1],
        'location': res[2]
    }
    cursor.close()
    return obj, 200


def search_book(isbn):
    '''Searches for the book locally and if
    not found searches for it on adlibris'''
    cursor = db.cursor()
    isbn = isbn.replace('-', '')
    # search locally
    query = 'SELECT COUNT(*) as counter FROM literature \
    WHERE ISBN10 = {0} OR ISBN13 = {0}'
    cursor.execute(query.format("'" + isbn + "'"))
    counter = cursor.fetchone()[0]
    if counter == 0:
        # find the book
        resp_data, resp_status = add_book(isbn)
        cursor.close()
        return resp_data, resp_status
    cursor.close()
    return get_book(isbn)


def add_book(isbn):
    '''Finds the book @ adlibris and
    takes the information down locally'''
    cursor = db.cursor()
    isbn = isbn.replace('-', '')
    # search for the book on adlibris
    literature, status_code = isbn_search.adlibris(isbn)
    if status_code == 400:
        return 'Literature could not be found on adlibris', status_code

    query_literature = 'INSERT INTO literature \
    VALUES({0}, {1}, {2}, {3})'
    query_author = 'call link_author({0}, {1})'
    try:
        cursor.execute(query_literature.format(
            "'" + literature['isbn10'] + "'",
            "'" + literature['isbn13'] + "'",
            "'" + literature['title'] + "'",
            "'" + literature['img'] + "'"
            ))
        for author in literature['author']:
            cursor.execute(query_author.format(
                "'" + author + "'",
                "'" + isbn + "'"))
            db.commit()
    except Exception, e:
        print str(e)
        cursor.close()
        return 'something went wrong: ' + str(e), 500
    cursor.close()
    return get_book(isbn)


def get_book(isbn):
    cursor = db.cursor()
    query_literature = 'SELECT * FROM literature \
    WHERE ISBN10 = {0} OR ISBN13 = {0}'
    query_authors = 'call get_authors({0})'.format(isbn)
    try:
        cursor.execute(query_literature.format("'" + isbn + "'"))
        res = cursor.fetchone()
        book = {
            'isbn10': res[0],
            'isbn13': res[1],
            'title': res[2],
            'img': res[3],
            'authors': []
        }
        cursor.execute(query_authors)
        res = cursor.fetchall()
        print res
        for item in res:
            book['authors'].append(item[0])
    except Exception, e:
        cursor.close()
        return str(e), 500
    cursor.close()
    return book, 200


def verify_login(user_name, password):
    '''Transform password to correct format
    and fetches the password from the user'''
    cursor = db.cursor()
    password = salted_password(password)
    query = 'SELECT user_password FROM student WHERE user_name = {0}'
    try:
        cursor.execute(query.format("'" + user_name + "'"))
        item = cursor.fetchone()
        if password == item[0]:
            cursor.close()
            return 'login successful', 200
        else:
            cursor.close()
            return 'login failed', 400
    except Exception, e:
        cursor.close()
        return 'invalid user name', 400


def salted_password(password):
    config.read(cfg_path)
    hashed_password = hashlib.sha512(
        password + config.get('Student_literature', 'hash_key')
    ).hexdigest()
    return hashed_password
