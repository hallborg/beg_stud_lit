'''Connection to mariadb
Import db and cursor to the application
for doing queries'''

import MySQLdb as mariadb
db = mariadb.connect(
    host='localhost',
    user='root',
    # passwd='root',
    db='Student_literature'
)

cursor = db.cursor()
# cursor.execute('SELECT * FROM USER')
# this will print the first name of a user in the db
# for item in cursor:
# print item
