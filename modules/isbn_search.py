import requests


def locally(isbn):

    return 'something'


def adlibris(isbn):
    if len(isbn) != 10 and len(isbn) != 13:
        return 'bad ISBN number', 400
    res = requests.get('http://www.adlibris.com/se/sok?q={0}'.format(isbn))
    full_text = res.text

    # find the block of the important information
    book_start = res.text.find('section search results') + 24
    book_end = res.text.find('</script>', book_start)
    book_text = full_text[book_start:book_end]
    # find the img url
    img_start = book_text.find('data-original=') + 15
    img_end = book_text.find('"', img_start)
    img = book_text[img_start:img_end]
    # find the title of the book
    title_start = book_text.find('<h3>') + 4
    title_end = book_text.find('</h3>', title_start)
    title = book_text[title_start:title_end]
    title = " ".join(title.split())
    # check if the response is valid
    if title_start is 3:
        return None, 400
    # finds the authors
    authors = []
    reading_point = title_end
    while book_text.find('filter=author', reading_point) != -1:
        reading_point = book_text.find('filter=author', reading_point) + 1
        author_start = book_text.find('>', reading_point) + 1
        author_end = book_text.find('</', author_start)
        authors.append(book_text[author_start:author_end])

    isbn13_start = book_text.find('ISBN', author_end) + 5
    isbn13_end = book_text.find('</', isbn13_start)
    isbn13 = book_text[isbn13_start:isbn13_end]

    if len(isbn) is not 10:
        isbn = '-'
    literature = {
        'title': title,
        'author': authors,
        'img': img,
        'isbn13': isbn13,
        'isbn10': isbn
    }
    return literature, 200
