from porc import Client
host = "https://api.aws-eu-west-1.orchestrate.io/"
client = Client("d34a94b7-4785-43d0-89b7-7f97cda3b569", host)


def create_user(user_name, password, email, phone_nr, location):
    response = client.put('users', user_name, {
      "user_name": user_name,
      "password": password,
      "email": email,
      "phone": phone_nr,
      "location": location
    })
    return response.status_code


def delete_user(user_name):
    response = client.delete('users', user_name)
    return response.status_code
