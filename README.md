# Second-hand Student Literature

A web application for selling and buying books for students (mainly at BTH@Sweden)
Based on ``Flask-Autodoc-Seed``

![alt text][logo]

[logo]: https://travis-ci.org/Hallborg/beg_stud_lit.svg

### Packages 
* Flask (Main package)
* jsonify (Converting data to be accessed via the API)
* make_response (HTTP response with easy modifiable status code)
* render_template (Easy way to show html docs)
* requests (HTTP for humans, simple way to do HTTP requests)
* Autodoc (Used for docing API)
* autodoc_json (converts the doc:ed api to json)

### TODO
Lets start with three things that are crutial to the application:

Simple GUI

Create user from GUI

Login
