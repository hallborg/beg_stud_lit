CREATE DATABASE IF NOT EXISTS Student_literature;
use Student_literature;
DROP PROCEDURE IF EXISTS create_user;
DROP PROCEDURE IF EXISTS get_authors;
DROP PROCEDURE IF EXISTS link_author;
DROP TABLE IF EXISTS lite_auth;
DROP TABLE IF EXISTS sale;
DROP TABLE IF EXISTS student;
DROP TABLE IF EXISTS literature;
DROP TABLE IF EXISTS author;

CREATE TABLE student (
user_name varchar(64),
user_password varchar(128),
email varchar(64),
phone_nr varchar(16),
location varchar(32),
primary key(user_name)
);

CREATE TABLE literature (
ISBN10 varchar(10),
ISBN13 varchar(13),
title varchar(256),
img varchar(256),
primary key(ISBN13)
);

CREATE TABLE author (
author_name varchar(64),
id int auto_increment not null,
primary key(id, author_name)
);

CREATE TABLE lite_auth (
author_id int,
ISBN13 varchar(13),
primary key(author_id, ISBN13),
foreign key(author_id) references author(id),
foreign key(ISBN13) references literature(ISBN13)
);

CREATE TABLE sale (
user_name varchar(64),
ISBN13 varchar(13),
price int not null,
foreign key(user_name) references student(user_name),
foreign	key(ISBN13) references literature(ISBN13)
);

delimiter //
CREATE PROCEDURE create_user (
user_name varchar(64),
user_password varchar(128),
email varchar(64),
phone_nr varchar(16),
location varchar(32)
)
begin
	insert into student values(user_name, user_password, email, phone_nr, location);
end //

delimiter //
CREATE PROCEDURE get_authors (
isbn varchar(13)
)
begin
	select author_name from author
    inner join lite_auth on author_id = id
    where ISBN13 = isbn;
end //

delimiter //
CREATE PROCEDURE link_author (
auth_name varchar(64),
isbn13 varchar(13)
)
begin
	set @id = IFNULL((select id from author where author_name = auth_name), 0);
    if (@id = 0) then
		insert into author(author_name)
		values (auth_name);
        set @id = (select id from author where author_name = auth_name);
	end if;
    insert into lite_auth(author_id, ISBN13)
    values(@id, isbn13);
end //
